// Users Table Command

CREATE TABLE users( 
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    email VARCHAR(100) NOT NULL,
    password VARCHAR(300) NOT NULL,
    datetime_created DATETIME
    );

// POSTS Table Command

CREATE TABLE posts (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    author_id INT NOT NULL,
    title VARCHAR(500) NOT NULL,
    content VARCHAR(5000) NOT NULL,
    datetime_posted DATETIME NOT NULL,
    
    CONSTRAINT fk_users_author_id
    	FOREIGN KEY(author_id) REFERENCES users(id)
    	ON UPDATE CASCADE
    	ON DELETE RESTRICT   
    );

// POST COMMENTS Table Commands

CREATE TABLE post_comments(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    post_id INT NOT NULL,
    user_id INT NOT NULL,
    content VARCHAR(5000) NOT NULL,
    datettime_commented DATETIME NOT NULL,
    
    CONSTRAINT fk_posts_post_id
    	FOREIGN KEY(post_id) REFERENCES posts(id)
    	ON UPDATE CASCADE
    	ON DELETE RESTRICT,
    
    CONSTRAINT fk_users_user_id
    	FOREIGN KEY(user_id) REFERENCES users(id)
    	ON UPDATE CASCADE
    	ON DELETE RESTRICT
    );

// POST LIKES Table Commands

CREATE TABLE post_likes( 
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    post_id INT NOT NULL,
    user_id INT NOT NULL,
    datetime_liked DATETIME NOT NULL,
    
    CONSTRAINT fk_posts_likes_posts
    	FOREIGN KEY(post_id) REFERENCES posts(id)
    	ON UPDATE CASCADE
    	ON DELETE RESTRICT,
    
    CONSTRAINT fk_users_likes_users
    	FOREIGN KEY(user_id) REFERENCES users(id)
    	ON UPDATE CASCADE
    	ON DELETE RESTRICT
    );